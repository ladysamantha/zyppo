const archiver = require('archiver');

const { Promise } = require('bluebird');

class ZipArchiver {
  constructor(stream) {
    this._stream = stream;

    // TODO: Add options later
    this._archive = archiver('zip');
    this._closeProm = new Promise((resolve) => {
      this._stream.on('close', () => {
        resolve();
      });
    });

    this._warnProm = new Promise((resolve, reject) => {
      this._archive.on('warn', (err) => {
        if (err.code === 'ENOENT') {
          resolve(err);
        } else {
          reject(err);
        }
      });
    });

    this._errProm = new Promise((_, reject) => {
      this._archive.on('error', (err) => reject(err));
    });

    this._archive.pipe(this._stream);
  }

  close() {
    return this._closeProm;
  }
}

module.exports = {
  ZipArchiver,
};
